#!/bin/bash

DBUSER=$1
DBPASS=$2

service mysql start

sleep 5

UP=$(pgrep mysql | wc -l);
if [ "$UP" -ne 1 ];
then
    service mysql start
fi

sh /root/scripts/prestashop-mysql.sh $DBUSER $DBPASS

/etc/init.d/apache2 start &&  tail -F -n 250 /var/log/*/*.log

