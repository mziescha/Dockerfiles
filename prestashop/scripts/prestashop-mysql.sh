#!/bin/bash

echo $1 >> /var/log/prestashop.log
echo $2 >> /var/log/prestashop.log

mysql -uroot -proot <<MYSQL_SCRIPT
CREATE DATABASE $1;
CREATE USER '$2'@'localhost' IDENTIFIED BY '$2';
GRANT ALL PRIVILEGES ON $1.* TO '$1'@'localhost';
FLUSH PRIVILEGES;
MYSQL_SCRIPT