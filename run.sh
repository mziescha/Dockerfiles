#!/bin/bash
 
#replace variables
NAMESPACE=mziescha
CONTAINER_NAME=cpanmini_test

while getopts "mw" opt; do
  case $opt in
    m)
      echo "-m was triggered for cpanmini!" >&2
      docker run -t -i --name $CONTAINER_NAME -v ./containers/:/root/mount $NAMESPACE/cpanmini
      ;;
    w)
      echo "-c was triggered for cpanminiweb!" >&2
      docker run -d --name $CONTAINER_NAME -p 8080:8080 -v ./containers/:/root/mount $NAMESPACE/cpanminiweb
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      ;;
  esac
done

