#!/bin/bash
 
while getopts "aciue" opt; do
    case $opt in
        a)
            echo "-a was triggered!" >&2
            docker rm $(docker ps -a -q)
            docker rmi $(docker images -q)
        ;;
        c)
            echo "-c was triggered!" >&2
            docker rm $(docker ps -a -q)
        ;;
        i)
            echo "-i was triggered!" >&2
            docker rmi $(docker images -q)
        ;;
        u)
            echo "-u was triggered!" >&2
            docker rmi $(docker images | grep "^<none>" | awk "{print $3}")
        ;;
        e)
            echo "-e remove all not exited containers!" >&2
            docker rm `docker ps -aq -f status=exited`
        ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
        ;;
    esac
done

