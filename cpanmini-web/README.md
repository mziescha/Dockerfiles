# docker image for CPAN::Mini::Webserver

Simple cpan mirror running with CPAN::Mini::Webserver based on CPAN::Mini

### Version
... :-D

### Tech

Running with:

* [Ubuntu:14.04] - HTML enhanced for web apps!
* [Perl:5.20.2] - awesome web-based text editor
* [CPAN::Mini::Webserver] - a super fast port of Markdown to JavaScript

### start container

```sh
$ docker run -d --name cpan_mini -p 8080:8080 -v /home/mziescha/workspace:/root/mount mziescha/cpanmini-web
```

### Todos

License
----
MIT

**Free Software, Hell Yeah!**

   [Ubuntu:14.04]: <http://www.ubuntu.com/>
   [Perl:5.20.2]: <https://www.perl.org/>
   [CPAN::Mini::Webserver]: <https://metacpan.org/pod/CPAN::Mini::Webserver>
