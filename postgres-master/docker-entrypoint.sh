#!/bin/sh

set -e

echo host replication all 0.0.0.0/0 trust >> /var/lib/postgresql/data/pg_hba.conf
psql -v ON_ERROR_STOP=1 --username postgres --dbname postgres -c "ALTER SYSTEM SET wal_level = hot_standby;"
psql -v ON_ERROR_STOP=1 --username postgres --dbname postgres -c "ALTER SYSTEM SET max_wal_senders = ${MAX_WAL_SENDERS};"
psql -v ON_ERROR_STOP=1 --username postgres --dbname postgres -c "ALTER SYSTEM SET max_replication_slots = ${MAX_REPLICATION_SLOTS};"
psql -v ON_ERROR_STOP=1 --username postgres --dbname postgres -c "ALTER SYSTEM SET wal_keep_segments = ${WAL_KEEP_SEGMENTS};"

exec gosu postgres pg_ctl -D "$PGDATA" -m fast -w stop
