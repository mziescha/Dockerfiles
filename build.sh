#!/bin/bash

ROOT_DIR='.'
BACKUP=false

#replace variables
AUTHOR='Mario Zieschang'
EMAIL=mziescha@cpan.org
NAMESPACE_USER=mziescha
NAMESPACE=mziescha

#prompt colors
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
RESET=`tput sgr0`

function run_ubuntu_base {
    echo "${GREEN}Running run_ubuntu_base with $1 ${RESET}" 
    if [ -z "${1}" ]; then 
        VERSION='14.04'
    else 
        VERSION=${1}
    fi
    array=( "base" )
    run  ${array[@]}
    echo "${YELLOW}run_ubuntu_base with $1 finished ${RESET}"
}

function run_perl_test {
    echo "${GREEN}Running run_perl_test with $1 ${RESET}" 
    if [ -z "${1}" ]; then 
        VERSION='16.04'
    else 
        VERSION=${1}
    fi
    run  "perl_tests"
    echo "${YELLOW}run_perl_test with $1 finished ${RESET}"
}

function run_cpanmini {
    run_ubuntu_base '14.04'
    array=( "perl" "lib-cpanmini" "cpanmini" )
    run  ${array[@]}
}

function run_cpanminiweb {
    run_cpanmini
    array=( "lib-cpanmini-web" "cpanmini-web" )
    run  ${array[@]}
}

function run_lib_postgres {
    echo "${GREEN}Running run_lib_postgres with $1 ${RESET}" 
    
    if [ $1 = 9.4 ]
    then
        run_ubuntu_base '15.10'
    elif [ $1 == 9.3 ]
    then
        run_ubuntu_base '14.04'
    else 
        run_ubuntu_base '14.04'
    fi

    array=( "lib-postgres" )
    run  ${array[@]}
    
    echo "${YELLOW}run_lib_postgres with $1 finished ${RESET}"
}

function run_postgresbase {
    echo "${GREEN}Running run_postgresbase with $1 ${RESET}" 
    run_lib_postgres $1
    array=( "postgres-base" )
    run  ${array[@]}
    echo "${YELLOW}run_postgresbase with $1 finished ${RESET}"
}

function run_postgres_master {
    run_lib_postgres $1
    array=( "postgres-master" )
    run  ${array[@]}
}

function run_postgres_slave {
    run_lib_postgres $1
    array=( "postgres-slave" )
    run  ${array[@]}
}

function run_jenkins {
    array=( "lib-jenkins" )
    run  ${array[@]}
}

function run() {
    LIBS=($@)
    for LIB in ${LIBS[@]}
    do
        echo "${GREEN}Running docker build -t ${NAMESPACE}/${LIB} ${ROOT_DIR}/${LIB} ${RESET}"
        cp "${ROOT_DIR}/${LIB}/Dockerfile.tt" "${ROOT_DIR}/${LIB}/Dockerfile"
        sed -i "s/\[%author%\]/$AUTHOR/g" "${ROOT_DIR}/${LIB}/Dockerfile"
        sed -i "s/\[%email%\]/<$EMAIL>/g" "${ROOT_DIR}/${LIB}/Dockerfile"
        sed -i "s/\[%namespace%\]/$NAMESPACE/g" "${ROOT_DIR}/${LIB}/Dockerfile"
        sed -i "s/\[%version%\]/$VERSION/g" "$ROOT_DIR/${LIB}/Dockerfile"
        sed -i "s/\[%namespace_user%\]/$NAMESPACE_USER/g" "$ROOT_DIR/${LIB}/Dockerfile"
        docker build -t ${NAMESPACE}/${LIB} ${ROOT_DIR}/${LIB}
        echo "${YELLOW}${NAMESPACE}/${LIB} finished ${RESET}"
    done
}

function _help() {
    echo "Possible options:"  >&2
    echo "-cm|--cpanmini "  >&2
    echo "-pg|--postgres-base "  >&2
    echo "-cmw|--cpanmini-web "  >&2
}

function main() {
    local COMMAND=$1
    shift

    # extract options and their arguments into variables.
    while true ; do
        case "$COMMAND" in
            -b|--backup)
                BACKUP=true
                shift
            ;;
            -b|--ubuntu-base)
                echo "-all was cpanmini-web with '$@'!" >&2
                run_ubuntu_base "$@"
                shift
            ;;
            -pt|--perl-test)
                echo "-all was cpanmini-web with '$@'!" >&2
                run_perl_test "$@"
                shift
            ;;
            -cmw|--cpanmini-web)
                echo "-cmw was cpanmini-web!" >&2
                run_cpanminiweb
                shift
            ;;
            -cm|--cpanmini)
                echo "-cm was cpanmini!" >&2
                run_cpanmini
                shift
            ;;
            -pg|--postgresbase)
                echo "-pg was postgres-base!" >&2
                run_postgresbase "$@"
                shift
            ;;
            -j|--jenkins)
                echo "-j was jenkins!" >&2
                run_jenkins "$@"
                shift
            ;;
            --) shift; break ;;
            *) 
                break
            ;;
        esac
    done
}

main "$@"