#!/usr/bin/perl

=head1 NAME

DBIx::NamedQueries::Driver::mysql - MySQL driver package for DBIx::NamedQueries handlings!

=head1 VERSION

Version 0.01

=cut

our $VERSION = '0.01';

use Getopt::Long qw{:config bundling};
use Archive::Tar;
use Class::Tiny qw/uri/;

use Carp;
use CPAN::Meta::YAML;
use CPAN::Meta::Requirements;
use HTTP::Tiny;
use DDP;

=head1 SUBROUTINES/METHODS

=cut

{

=head2 _h_process_command_line

Private subroutine to handle command line args.

=cut

    sub _h_process_command_line {
        my (%params) = @_;
        $params{'argv'} = [@ARGV];
        GetOptions(

            # command
            'h|?|help'       => \$params{help},
            'v|verbose'      => \$params{verbose},
            's|showonly'     => \$params{showonly},
            'd|dependencies' => \$params{dependencies},

            # options
            'f|file=s'     => \$params{file},
            'i|ignore=s'   => \$params{ignore},
            'm|mirror=s'   => \$params{mirror},
            'a|authorid=s' => \$params{authorid},

        );
        return %params;
    }

sub _search_packages {
    my ($args) = @_;
    require BackPAN::Index;
    my $backpan = BackPAN::Index->new;
    my $name    = $args->name;
    $name =~ s/::/-/;
    p $name;

    # These are all DBIx::Class::ResultSet's
    my $files    = $backpan->files;
    my $dists    = $backpan->dists;
    my $releases = $backpan->releases($name);
    my $release  = ( $args->version > 0 ) ? $releases->single( { version => $args->version } ) : $releases->all;
    p $args;
    p $args->version;
    p $release;

    unless ($release) {
        p $releases;
        die;
    }

    # p $args;
    # p $args->depth;
    # p $args->name;
    # p $args->distribution;
    # p $args->version;

    return;
}
}

=head2 run

Collection of App::NamedQuery packages to run on command line.

=cut

sub run {
    my $self = shift;
    my %args = ();
    %args = _h_process_command_line(%args);
    pod2usage( { -verbose => 1 } ) if ( $args{help} );

    require File::HomeDir;
    my $home         = File::HomeDir->my_home;
    my $mirror       = $args{mirror} || 'http://www.cpan.org/authors/id/';
    my $package_file = $args{file} || die 'File is required!!';
    my $author_id    = $args{authorid} || die 'Author ID is required!!';
    my $tar          = Archive::Tar->new;

    $tar->read( $package_file, 1 );
    my $content = undef;
    foreach my $file ( $tar->list_files ) {
        next if ( $file !~ /META\.json/ );
        require JSON;
        $content = JSON::from_json( $tar->get_content($file) );
        last;
    }

    die qq~'Unable to read META.json in file $package_file'~ unless $content;

    # track dependencies from goven module
    my $modules = {};

    require CPAN::FindDependencies;
    foreach my $prereq ( keys %{ $content->{prereqs} } ) {
        foreach my $dependency ( keys %{ $content->{prereqs}->{$prereq}->{requires} } ) {
            p $dependency if $args{verbose};
            foreach ( CPAN::FindDependencies::finddeps($dependency) ) {
                my $package = _search_packages($_);
                if ( $args{verbose} ) {
                    my $s_module = qq~$_->{cpanmodule} => ~ . $_->version;

                    #p $s_module;
                    #p $package;
                    #p $_->distribution;
                }
                $modules->{ $_->name }->{ $_->distribution } = $_->version;
            }
        }
    }
    return if $args{showonly};

    require CPAN::Mini::Inject;
    my $mcpi = CPAN::Mini::Inject->new;
    $mcpi->parsecfg( $home . '/.mcpani/config' );

    #add dependencies from given package to cpan mini
    if ( $args{dependencies} ) {
        require LWP::Simple;
        foreach my $module_name ( keys %$modules ) {
            foreach my $module_package ( keys %{ $modules->{$module_name} } ) {
                my ( $first_prefix, $second_prefix, $authorid, $file ) = ( split '/', $module_package );
                next if ( defined $args{ignore} && $module_package =~ /$args{ignore}/ );
                my $local_tmp_file = '/tmp/' . $file;
                LWP::Simple::getstore( $mirror . $module_package, $local_tmp_file );
                $mcpi->add(
                    module   => $module_name,
                    authorid => $authorid,
                    version  => $modules->{$module_name}->{$module_package},
                    file     => $local_tmp_file,
                );
                $mcpi->inject;
                unlink $local_tmp_file;
            }
        }
    }

    #add given package to cpan mini
    $content->{name} =~ s/-/::/;
    $mcpi->add(
        module   => $content->{name},
        authorid => $author_id,
        version  => $content->{version},
        file     => $package_file
    );
    $mcpi->inject;
}

run;

# namedquery

__END__


=head1 SYNOPSIS

    $>mcpanideps -f file.tar.gz -a AUTHORID -i '\/(perl-.*)'
    
=head1 OPTIONS

=over 4

    Commands:
        'h|?|help' | Prints what you see
    

    Options:
        f|file     | (required) path to file to import
        a|authorid | (required) cpan like author id
        i|ignore   | ingnor regex which dependencies should not be installed
        m|mirror   | mirror to download dependencies

=back

=head1 AUTHOR

Mario Zieschang, C<< <mario.zieschang at unitedprint.com> >>

=head1 BUGS


=head1 SUPPORT

You can also look for information at:

=over 4

=back


=head1 ACKNOWLEDGEMENTS


=head1 LICENSE AND COPYRIGHT

=cut

1;
